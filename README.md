# Building flask application

## Getting started

### 1. Create a fork of this project.

Go to this [repository homepage](https://gitlab.com/startx-demo/devops/python-flask) and click on the [fork button](https://gitlab.com/startx-demo/devops/python-flask/-/forks/new).

### 2. clone your project

When you are under you own copy of the repository, click on the clone button, and copy the http url to download your repository

```bash
git clone https://gitlab.com/<your_username>/python-flask.git
cd python-flask
```

### 3. create your personnal branch

```bash
git checkout -b your_name
```

### 4. Configure your python virtual environment

Create a project folder and a **pyenv** folder within

```bash
python3 -m venv pyenv
```

Activate your virtual environement

```bash
source pyenv/bin/activate
```

Ignore the content of your python virtual environement (should be recreated in each running environement)

```bash
echo 'pyenv' > .gitignore
```

Update you pip and python environement

```bash
python3 -m pip install --upgrade pip
```

### 4. Install your python requirements

Create the **requirements.txt** file.

```bash
touch requirements.txt
```

Add the flask dependency

```bash
echo 'Flask>=2.1.1' >> requirements.txt
```

Commit you changes

```bash
git add .
git commit -m "Adding Flask requirements"
```

Add 2 other library (used to speed dev environement)

```bash
echo 'blinker>=1.4' >> requirements.txt
echo 'watchdog>=2.1.7' >> requirements.txt
echo 'python-dotenv>=0.20.0' >> requirements.txt
```

Commit you changes

```bash
git add .
git commit -m "Adding blinker and watchdog requirements"
```

Install the flask library

```bash
pip install -r requirements.txt 
```

Check that no change where tracked in your working copy

```bash
git status
# should return no change
```

### 5. Code your entrypoint

Create a **home.py** file

```bash
touch home.py
vi home.py
```

Import you Flask library and create a first function answering to the **/** path.

```python
from flask import Flask
app = Flask(__name__)

@app.route('/')
def index():
    return '<h1>Index Page</h1>'
```

### 6. Create you development environment

Create a **.env** file

```bash
touch .env
```

Declare your flask application in environment variables

```bash
FLASK_APP=home
FLASK_ENV=development
```

Start your development server

```bash
flask run --host=0.0.0.0
```

Check if your application is running (in another terminal)

```bash
curl http://localhost:5000/
```

Stop you development server

```bash
ctrl+C on your terminal running the flask server
```

Commit you changes

```bash
git add .env
git commit -m "Adding environment variables for the development mode"
git add home.py
git commit -m "Adding the first route for /"
```

### 7. Improve your code

Start your development server (in a dedicated terminal)

```bash
flask run --host=0.0.0.0
```

Add a new **hello** endpoint to your application

```python

@app.route('/hello')
def hello():
    return 'Hello, World'
```

Check if your application is exposing this new endpoint

```bash
curl http://localhost:5000/hello
```

Commit you changes

```bash
git commit -am "Adding the second route for /hello"
```


Add a new **hello** endpoint to your application

```python

@app.route('/user/<username>')
def profile(username):
    return f'{username}\'s profile'
```

Check if your application is exposing this new endpoint

```bash
curl http://localhost:5000/user/christophe
```

Commit you changes

```bash
git commit -am "Adding the third route for /user/<username>"
```

## Source

This lab is based on the [pallets quickstart](https://flask.palletsprojects.com/en/2.1.x/quickstart). Read it for more examples.
